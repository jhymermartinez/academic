// Invocar modo JavaScript 'strict'
'use strict';

// Configurar el módulo routes de 'articles'
angular.module('reference').config(['$routeProvider',
	function($routeProvider) {
		$routeProvider
		.when('/reference/create', {
			templateUrl: 'reference/views/create-reference.client.view.html'
		});
	}
]); 