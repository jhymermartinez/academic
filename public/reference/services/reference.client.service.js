// Invocar modo JavaScript 'strict'
'use strict';

// Crear el service 'articles'
angular.module('reference').factory('Reference', ['$resource',
 function($resource) {
	// Usar el service '$resource' para devolver un objeto '$resource' article
    return $resource('api/reference/:referenceId', {
        referenceId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);