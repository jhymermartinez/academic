// Invocar modo JavaScript 'strict'
'use strict';

// Crear el controller 'articles'
angular.module('reference').controller('ReferenceController', ['$scope', '$routeParams', '$location','Reference',
    function($scope, $routeParams, $location,Reference) {


     // Crear un nuevo método controller para crear nuevos articles
        $scope.create = function() {
            // Usar los campos form para crear un nuevo objeto $resource article
            var reference = new Reference({
                name: this.name,
                location: this.location,
                description: this.description,
                category: this.category
            });

            // Usar el método '$save' de article para enviar una petición POST apropiada
            reference.$save(function(response) {
                // Si un artículo fue creado de modo correcto, redireccionar al usuario a la página del artículo 
                $location.path('/');
            }, function(errorResponse) {
                // En otro caso, presentar al usuario el mensaje de error
                $scope.error = errorResponse.data.message;
            });
        };

    }
]);