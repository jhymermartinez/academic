// Invocar modo JavaScript 'strict'
'use strict';

// Cargar las dependencias del módulo
var reference = require('../../app/controllers/reference.server.controller');

// Definir el método routes de module
module.exports = function(app) {
	// Configurar la rutas base a 'articles'  
	app.route('/api/reference')
	   .get(reference.list)
	   .post(reference.create);

};







