var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ReferenceSchema = new Schema({
  
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'El nombre no puede quedar vacio'
  },
  location: {
    type: String,
    default: '',
    trim: true,
    required: 'Debe colocar una dirección válida'
  },
  description: {
    type: String,
    default: '',
    trim: true,
  },
  category: {
    type: String,
    enum:['blog','video','doc','news','web page','test']
  }
});

mongoose.model('Reference', ReferenceSchema);